package stepDefinitions;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostmanSteps {
	RequestSpecification requstSepecification;
	Response response;
	
	@Given("^acessar a url$")
	public void acessar_a_url() throws Throwable {
		requstSepecification = given()
		.relaxedHTTPSValidation()
		.param("foo1", "bar1")
		.param("foo2", "bar2");
	}

	@When("^enviar a requisicao$")
	public void enviar_a_requisicao() throws Throwable {
		String uriBase = "https://postman-echo.com/get";
		response = requstSepecification.when().get(uriBase);
	}

	@Then("^validar requisicao enviada com sucesso$")
	public void validar_requisicao_enviada_com_sucesso() throws Throwable {
	   Assert.assertEquals(response.getStatusCode(), 200);
	}
}
